import string

def _generate_codes(digit, codes):
    """Generator that yields symbol by symbol for conversion
    like DEC -> Hex
    """
    codes_len = len(codes)
    while digit:
        mod = digit % codes_len
        digit = digit // codes_len
        yield codes[mod-1]

def digit_to_code(digit):
    """ Returns code represantation of integer
    based on digits and ascii letters

    Args:
        digit (int): input digit that should be converted
    Returns:
        string: representation string for digit

    """
    code_chars = string.ascii_letters + string.digits
    char_arrays = _generate_codes(digit, code_chars)
    char_arrays = reversed(list(char_arrays))
    char_arrays = ''.join(char_arrays)
    return char_arrays
