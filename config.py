import os
from os import environ

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
SECRET_KEY = environ.get('SECRET_KEY')

SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI')
SQLALCHEMY_MIGRATE_REPO = os.path.join(BASE_DIR, 'db_repository')
DEBUG = environ.get('DEBUG', True)
if DEBUG:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR,
                                                              'app.db')

BABEL_DEFAULT_LOCALE = 'en'
