from flask import Flask, jsonify, request, render_template, redirect
from flask_babel import gettext as _
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from extensions import db, babel
from models import Url, Click
from utils import digit_to_code


app = Flask(__name__)
app.config.from_object('config')
babel.init_app(app)
db.init_app(app)
db.app = app
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

@app.route('/api/short_url', methods=['POST'])
def short_url():
    data = {}
    url_string = request.json.get('url')
    if url_string:
        url = Url()
        url.url = url_string
        db.session.add(url)
        db.session.commit()
        url.code = digit_to_code(url.id)
        db.session.add(url)
        db.session.commit()
        return jsonify({
            'url': '{0}{1}'.format(request.url_root, url.code),
            'code': url.code
        })
    else:
        data['error'] = _('url is missing')
        return jsonify(data), 400

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/<code>')
def follow_url(code):
    url = Url.query.filter_by(code=code).first_or_404()
    click = Click()
    click.url_id = url.id
    click.info = request.user_agent.string
    db.session.add(click)
    db.session.commit()
    redirect_url = url.url
    if not redirect_url.startswith('http'):
        redirect_url = 'http://{0}'.format(redirect_url)
    return redirect(redirect_url)

@app.route('/statistic/<code>')
def statistic(code):
    url = Url.query.filter_by(code=code).first_or_404()
    clicks = Click.query.filter_by(url_id=url.id).count()
    data = {
        'url': '{0}{1}'.format(request.url_root, url.code),
        'clicks': clicks
    }
    return render_template('statistic.html', **data)


if __name__ == '__main__':
    manager.run()
