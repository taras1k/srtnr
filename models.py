from extensions import db


class Url(db.Model):
    """
    Model for Questions
    """
    __tablename__ = 'urls'
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.Text)
    code = db.Column(db.String(255), index=True, unique=True)
    created = db.Column(db.DateTime, default=db.func.now())
    updated = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

class Click(db.Model):
    """
    Model for tracking visits of each url
    """
    __tablename__ = 'clicks'
    id = db.Column(db.Integer, primary_key=True)
    info = db.Column(db.Text)
    url_id = db.Column(db.Integer, db.ForeignKey('urls.id'))
    created = db.Column(db.DateTime, default=db.func.now())
