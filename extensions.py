from flask_babel import Babel
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
babel = Babel(default_locale='en')
